package net.danilos.functional;

import net.danilos.arity.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class IntegralFunctionalTest {
    @Test
    public void testFunctionalLin() {
        IntegralFunctional fun = new IntegralFunctional(0, 1);
        Func lin = new LinFunc(1, 0, 0, 1);
        assertEquals(0.5, fun.functional(lin), 0.1);
    }

    @Test
    public void testFunctionalDiv() {
        IntegralFunctional fun = new IntegralFunctional(0, 1);
        Func div = new DivFunc(1, 0, 0, 1, 0, 1);
        assertEquals(0.5, fun.functional(div), 0.1);
    }

    @Test
    public void testFunctionalSin() {
        IntegralFunctional fun = new IntegralFunctional(0, Math.PI/2.);
        Func sin = new SinFunc(2, 1, 0, 4);
        System.out.println(fun.functional(sin));
        //assertEquals(2,fun.functional(sin), 0.1);
        assertEquals(2,fun.functional(sin), 1e-3);
    }
}
