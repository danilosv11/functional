package net.danilos.functional;

import net.danilos.arity.DivFunc;
import net.danilos.arity.Func;
import net.danilos.arity.LinFunc;
import net.danilos.arity.SinFunc;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SumFunctionalTest {
    @Test
    public void testBoundsLin(){
        SumFunctional func = new SumFunctional(0,1);
        Func lin = new LinFunc(1,1,-1,2);
        assertEquals(4.5,func.functional(lin),0.1);
    }
    @Test
    public void testBoundsDiv(){
        SumFunctional func = new SumFunctional(0,1);
        Func div = new DivFunc(1,1,1,1,-1,2);
        assertEquals(3.0,func.functional(div),0.1);
    }
    @Test
    public void testBoundsSin(){
        SumFunctional func = new SumFunctional(0,1);
        Func sin = new SinFunc(1,1,-1,2);
        assertEquals(1.3,func.functional(sin),0.1);
    }

}
