package net.danilos.arity;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ArityTest {
    @Test
    public void testLinearArity() {
        Func lin = new LinFunc(1, 1, 0, 3);
        assertEquals(lin.getValueAtPoint(1), 2, 10E-9);
    }

    @Test(expected = RuntimeException.class)
    public void testDivArity() {
        Func div1 = new DivFunc(1, 1, 0, 0, 0, 8);
        div1.getValueAtPoint(1);

    }

    @Test
    public void testDivArityV2() {
        Func div1 = new DivFunc(2, 3, 2, 3, -1, 1);
        assertEquals(div1.getValueAtPoint(0), 1, 10E-9);
    }


    @Test
    public void testSinArity() {
        Func sin = new SinFunc(1, 1, 0, 1);
        assertEquals(sin.getValueAtPoint(0), 0, 10E-9);
    }

    @Test
    public void testExpArity() {
        Func ex1 = new ExpFunc(1, 1, 0, 4);
        assertEquals(3.7, ex1.getValueAtPoint(1), 0.1);
        Func ex2 = new ExpFunc(1, 1, 0, 4);
        assertEquals(2, ex2.getValueAtPoint(0), 10E-9);
    }
}
