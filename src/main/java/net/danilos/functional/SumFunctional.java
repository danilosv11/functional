package net.danilos.functional;

import net.danilos.arity.Func;

public class SumFunctional implements PolinomialFunctional {
    private double leftBound;
    private double rightBound;

    public SumFunctional(double leftBound, double rightBound) {
        this.leftBound = leftBound;
        this.rightBound = rightBound;
    }

    @Override
    public double functional(Func func) {
        if (func.getLeft() > leftBound  || rightBound > func.getRight()) {
            throw new IndexOutOfBoundsException();
        }
        return func.getValueAtPoint(leftBound) + func.getValueAtPoint(rightBound) + func.getValueAtPoint((rightBound + leftBound) / 2);
    }
}
