package net.danilos.functional;

import net.danilos.arity.Func;

public class IntegralFunctional implements PolinomialFunctional {
    private double leftBound;
    private double rightBound;

    public IntegralFunctional(double leftBound, double rightBound) {
        this.leftBound = leftBound;
        this.rightBound = rightBound;
    }

    public double functional(Func func) {
        if (func.getLeft() > leftBound  || rightBound > func.getRight()) {
            throw new IndexOutOfBoundsException();
        }
        double h = (rightBound - leftBound) / 10000;
        double ans = 0;
        for (double i = 0; i < 10000; i ++) {
            ans += func.getValueAtPoint(leftBound + h*i)*h;
        }
        return ans;
    }
}
