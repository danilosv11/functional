package net.danilos.functional;

import net.danilos.arity.Func;

public interface PolinomialFunctional<T extends Func>{
    double functional(T func);
}
