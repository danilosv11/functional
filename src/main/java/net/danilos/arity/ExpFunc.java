package net.danilos.arity;

public class ExpFunc implements Func {
    //f(x) = Aexp(x)+b;
    private double a;
    private double b;
    private double left;
    private double right;

    public ExpFunc(double a, double b, double left, double right) {
        this.a = a;
        this.b = b;
        this.left = left;
        this.right = right;
    }

    public double getValueAtPoint(double x) {
        if (x > right || x < left) {
            throw new RuntimeException();
        }
        return a * Math.exp(x) + b;
    }

    @Override
    public double getLeft() {
        return left;
    }

    @Override
    public double getRight() {
        return right;
    }
}
