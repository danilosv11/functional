package net.danilos.arity;

public class SinFunc implements Func {
    //f(x)=Asin(Bx);
    private double a;
    private double b;
    private double left;
    private double right;

    public SinFunc(double a, double b, double left, double right) {
        this.a = a;
        this.b = b;
        this.left = left;
        this.right = right;
    }

    public double getValueAtPoint(double x) {
        if (x > right || x < left) {
            throw new RuntimeException();
        }
        return a * Math.sin(b * x);
    }

    @Override
    public double getLeft() {
        return left;
    }

    @Override
    public double getRight() {
        return right;
    }
}
