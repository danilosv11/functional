package net.danilos.arity;

public interface Func {
    double getValueAtPoint(double x);
    double getLeft();
    double getRight();
}
